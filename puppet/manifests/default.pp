class webserver {

  $mysql_password = "vagrant"
  $mysql_user = "root"

  service { 'apache2':
    ensure  => running,
    enable => true,
  }

    service { 'php7.2-fpm':
      ensure  => running,
      enable => true,
    }

  #enable php-fpm config
      exec { "a2enmod_php-fpm":
        command => "/usr/sbin/a2enconf php7.2-fpm",
        notify  => Service['apache2'],
      }

  #enable rewrite module
    exec { "a2enmod_rewrite":
      command => "/usr/sbin/a2enmod rewrite",
      notify  => Service['apache2'],
    }

  #enable virtual hosts
    exec { "a2enmod_vhosts":
      command => "/usr/sbin/a2enmod vhost_alias",
      notify  => Service['apache2'],
    }

    #enable fastcgi proxy
        exec { "a2enmod_proxy_fcgi":
          require => Service['php7.2-fpm'],
          command => "/usr/sbin/a2enmod proxy_fcgi",
          notify  => Service['apache2'],
        }

  # testproject site
  file { '/etc/apache2/sites-available/testproject.conf':
    source  => '/vagrant/puppet/files/apache2/sites-available/testproject.conf',
    notify  => Service['apache2'],
  }
  file { "/etc/apache2/sites-enabled/testproject.conf":
    require => File["/etc/apache2/sites-available/testproject.conf"],
    ensure => "link",
    target => "/etc/apache2/sites-available/testproject.conf",
    notify => Service["apache2"]
  }


  host {'testproject.local':
    name => 'testproject.local',
    ensure => 'present',
    ip => '127.0.0.1',
    target => '/etc/hosts'
  }

  class { '::mysql::server':
    root_password           => $mysql_password,
    remove_default_accounts => true
  }

  file { "/etc/mysql/conf.d/mysqld.cnf":
    source => "/vagrant/puppet/files/mysql/mysqld.cnf",
    notify => Service['mysqld']
  }

  Mysql::Db { 'testproject':
    user     => $mysql_user,
    password => $mysql_password,
    host     => 'localhost',
    grant    => ['ALL'],
  }

  Mysql_user{ "$mysql_user@10.0.2.2":
    ensure        => present,
    password_hash => mysql_password($mysql_password),
    require       => Class['mysql::server'],
  }

  Mysql_grant { "$mysql_user@10.0.2.2/*.*":
    privileges => ['ALL'],
    table      => '*.*',
    user       => "$mysql_user@10.0.2.2",
  }

  exec { "mysql-import-default":
    path => ["/bin", "/usr/bin"],
    command => "mysql -u$mysql_user -p$mysql_password testproject < /vagrant/sql/testproject.sql",
    require => Mysql::Db["testproject"],
  }

  package { 'php7.2-mysql':
    ensure => present,
    require  => Class['::mysql::server'],
    notify  => Service['apache2'],
  }

  package { 'phpmyadmin':
    ensure => present,
    notify  => Service['apache2'],
  }

    exec { "phpmyadmin-chmod":
      command => "/bin/chown -R www-data:www-data /var/lib/phpmyadmin",
      notify  => Service['apache2'],
    }


  file_line { 'phpmyadmin.conf':
    path => '/etc/apache2/apache2.conf',
    line => 'Include /etc/phpmyadmin/apache.conf',
    notify  => Service['apache2'],
  }

  package { 'php-xdebug':
    ensure => present,
    notify  => Service['apache2'],
  }

  exec { "php7.2-xdebug-phpenmod":
    command => "/bin/bash -c \"phpenmod xdebug\"",
    notify  => Service['apache2'],
  }

  file_line { 'php7.2-xdebug-config':
    path => '/etc/php/7.2/fpm/php.ini',
    line => "xdebug.remote_enable=1;\nxdebug.remote_host=192.168.56.1;",
    notify  => Service['php7.2-fpm'],
  }

  exec { "php-ini-cli-timezone":
    command => "/bin/sed -i \"s/^;date.timezone =\$/date.timezone = \\\"Europe\\/Amsterdam\\\"/\" /etc/php/7.2/cli/php.ini",
    notify  => Service['apache2'],
  }

  exec { "php-ini-php-fpm-timezone":
    command => "/bin/sed -i \"s/^;date.timezone =\$/date.timezone = \\\"Europe\\/Amsterdam\\\"/\" /etc/php/7.2/fpm/php.ini",
    notify  => Service['php7.2-fpm'],
  }

    exec { "php-fpm-user":
      command => "/bin/sed -i \"s|^user = www-data$|user = vagrant|\" /etc/php/7.2/fpm/pool.d/www.conf",
      notify  => Service['php7.2-fpm'],
    }

    exec { "php-fpm-group":
      command => "/bin/sed -i \"s|^group = www-data$|group = vagrant|\" /etc/php/7.2/fpm/pool.d/www.conf",
      notify  => Service['php7.2-fpm'],
    }


    exec { "php-fpm-sessions":
      command => "/bin/chown -R vagrant:vagrant /var/lib/php/sessions"
    }

  class { '::composer':
    command_name => 'composer',
    target_dir   => '/vagrant',
    auto_update => false,
    user => "vagrant",
  }

  exec { "project-update":
    cwd => "/vagrant",
    path => ["/vagrant", "/usr/bin"],
    user => "vagrant",
    command => "/bin/bash -c \"export COMPOSER_HOME=/vagrant; php composer global require 'fxp/composer-asset-plugin:~1.3'; php composer create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic \.\"",
    require => Class["::composer"]
  }

}


include webserver