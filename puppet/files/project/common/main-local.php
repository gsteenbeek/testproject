<?php

return [
    'components' => [
        'webcare' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=stoneart_webcare',
            'emulatePrepare' => true,
            'username' => 'stoneart',
            'password' => 'vagrant',
            'charset' => 'utf8',
        ]
    ]

];