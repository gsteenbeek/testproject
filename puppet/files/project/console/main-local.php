<?php
return [
	'bootstrap' => [
	],
	'modules' => [
	],
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host='.$params['DB_LOCATION'].';dbname=stoneart_nieuwbouwmanager_nl',
			'emulatePrepare' => true,
			'username' => 'stoneart',
			'password' => 'vagrant',
			'charset' => 'utf8',
		],
		'webcare' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host='.$params['DB_LOCATION'].';dbname=stoneart_webcare',
			'emulatePrepare' => true,
            'username' => 'stoneart',
            'password' => 'vagrant',
			'charset' => 'utf8',
		],
	],

];
